import React from "react";
import "./App.css";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Games6 from "./components/game/game6";
import Games7 from "./components/game/game7";
import Dashboard from "./components/dashboard";

function App() {
  return (
    <Router>
      <div className="col-5">
        <Switch>
          <Dashboard exact path="/" component={Dashboard} />
          <Route exact path="/game6" component={Games6} />
          <Route exact path="/game7" component={Games7} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
