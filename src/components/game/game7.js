import React, { Fragment, useEffect, useState } from "react";
import "./styleGame.css";

class Games7 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      hasil: "",
      scorePlayer: 0,
      scoreKomputer: 0,
      bodi: {
        scorePlayer: 0,
        scoreKomputer: 0,
      },
    };
    this.mainkan = this.mainkan.bind(this);
    this.getComp = this.getComp.bind(this);
    this.getHasil = this.getHasil.bind(this);
    this.getScore = this.getScore.bind(this);
    this.onSubmit2 = this.onSubmit2.bind(this);
  }

  // Select Computer Choice Random
  getComp() {
    const comp = Math.random();
    if (comp < 0.34) return "BATU";
    if (comp >= 0.34 && comp < 0.67) return "GUNTING";
    return "KERTAS";
  }

  // Get Result (MENANG, KALAH, SERI)
  getHasil(pilComp, pilPlayer) {
    if (pilPlayer === pilComp) return "SERI";
    if (pilPlayer === `BATU`) return pilComp === `KERTAS` ? "KALAH" : "MENANG";
    if (pilPlayer === `GUNTING`)
      return pilComp === `KERTAS` ? "MENANG" : "KALAH";
    if (pilPlayer === `KERTAS`) return pilComp === `BATU` ? "MENANG" : "KALAH";
  }

  // Update Score
  getScore(hasil) {
    if (hasil === "SERI") {
      this.setState((tambah) => ({
        bodi: {
          ...tambah.bodi,
          scorePlayer: this.state.bodi.scorePlayer,
          scoreKomputer: this.state.bodi.scoreKomputer,
        },
      }));
    }
    if (hasil === "MENANG") {
      this.setState((tambah) => ({
        bodi: {
          ...tambah.bodi,
          scorePlayer: tambah.bodi.scorePlayer + 1,
        },
      }));
    }
    if (hasil === "KALAH") {
      this.setState((tambah) => ({
        bodi: {
          ...tambah.bodi,
          scoreKomputer: tambah.bodi.scoreKomputer + 1,
        },
      }));
    }
  }

  // Play Game
  mainkan(element) {
    // element.target.classList.add("terpilih");

    const pilComp = this.getComp();
    const pilPlayer = element.target.id;

    setTimeout(() => {
      const hasil = this.getHasil(pilComp, pilPlayer); // untuk mendapatkan hasil di dalam fungsi saja dan dilempar ke fungsi getScore di baris bawah ini , bukan untuk ditaruh di state
      this.getScore(hasil);
    }, 100);

    this.setState({
      hasil: this.getHasil(pilComp, pilPlayer),
    });
  }

  // Submit Score
  async onSubmit2(e) {
    window.location.reload(true);
  }

  render() {
    return (
      <Fragment>
        <section style={{ borderRadius: "20px", padding: "10px" }}>
          {/* <!-- ========== NAVIGASI ========== --> */}
          <nav className="navbar navbar-expand-lg">
            <a href="/">
              <img
                src={process.env.PUBLIC_URL + "/imgGame/arrow.png"}
                style={{
                  width: 30,
                  height: 30,
                  marginRight: 20,
                  marginLeft: 20,
                }}
                alt=""
              />
            </a>

            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
              <img
                src={process.env.PUBLIC_URL + "/imgGame/logo 1.png"}
                style={{ width: 40, height: 40, marginRight: 20 }}
                alt=""
              />
              <h3
                style={{
                  fontFamily: "arial",
                  fontWeight: "bolder",
                  fontSize: 20,
                  marginTop: 5,
                  color: "rgb(230, 190, 80)",
                }}
              >
                ROCK PAPER SCISSORS
              </h3>
            </div>
          </nav>
          {/* <!-- ========== END NAVIGASI ========== --> */}

          {/* <!-- ========== MAIN PAGE ========== -->         */}
          <div className="container">
            <div className="row" style={{ marginTop: 10 }}>
              {/* <!-- == player == --> */}
              <div className="col-4 my-3">
                <div className="row">
                  <div className="col-7 ml-auto">
                    <h2
                      style={{
                        fontFamily: "helvetica",
                        fontWeight: "bolder",
                        fontSize: "calc(.7rem + 0.8vw)",
                        color: "rgb(230, 190, 80)",
                      }}
                    >
                      ME
                    </h2>
                  </div>
                </div>

                <ul
                  className="listplayer"
                  style={{
                    listStyleType: "none",
                    textAlign: "right",
                    marginLeft: -15,
                  }}
                >
                  <li>
                    <img
                      className="btn-float"
                      id="BATU"
                      src={process.env.PUBLIC_URL + "/imgGame/batu.png"}
                      alt=""
                      onClick={this.mainkan}
                    />
                  </li>
                  <li>
                    <img
                      className="btn-float"
                      id="KERTAS"
                      src={process.env.PUBLIC_URL + "/imgGame/kertas.png"}
                      alt=""
                      onClick={this.mainkan}
                    />
                  </li>
                  <li>
                    <img
                      className="btn-float"
                      id="GUNTING"
                      src={process.env.PUBLIC_URL + "/imgGame/gunting.png"}
                      alt=""
                      onClick={this.mainkan}
                    />
                  </li>
                </ul>
              </div>
              {/* <!-- == end player == --> */}

              {/* <!-- == Hasil == --> */}
              <div className="col-4">
                <div
                  className="hasil"
                  id="hasil2"
                  style={{
                    marginLeft: "auto",
                    textAlign: "center",
                    marginTop: "170px",
                  }}
                >
                  {this.state.hasil}
                </div>
              </div>
              {/* <!-- == end Hasil == --> */}

              {/* <!-- == computer == --> */}
              <div className="col-4 my-3">
                <div className="row">
                  <div className="col-6 ml-3">
                    <h2
                      style={{
                        fontFamily: "helvetica",
                        fontWeight: "bolder",
                        fontSize: "calc(.7rem + 0.8vw)",
                        color: "rgb(230, 190, 80)",
                      }}
                    >
                      COM
                    </h2>
                  </div>
                </div>

                <ul
                  className="listcomp"
                  style={{ listStyleType: "none", marginLeft: -35 }}
                >
                  <li>
                    <img
                      id="BATU"
                      src={process.env.PUBLIC_URL + "/imgGame/batu.png"}
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      id="KERTAS"
                      src={process.env.PUBLIC_URL + "/imgGame/kertas.png"}
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      id="GUNTING"
                      src={process.env.PUBLIC_URL + "/imgGame/gunting.png"}
                      alt=""
                    />
                  </li>
                </ul>
              </div>
              {/* <!-- == end computer == --> */}
            </div>
          </div>
          {/* <!-- ========== END MAIN PAGE ========== --> */}

          {/* <!-- ========== REFRESH ========== --> */}
          <div className="row text-center" style={{ marginTop: "-25px" }}>
            <input
              className="text-center"
              style={{
                height: 25,
                width: 50,
                fontWeight: "bold",
                backgroundColor: "white",
                borderRadius: "6px",
                border: "solid 1px white",
                marginLeft: "auto",
                marginRight: "15px",
              }}
              type="integer"
              name="scorePlayer"
              placeholder="Your Score"
              value={this.state.bodi.scorePlayer}
            />

            <input
              className="text-center"
              style={{
                height: 25,
                width: 50,
                fontWeight: "bold",
                backgroundColor: "white",
                borderRadius: "6px",
                border: "solid 1px white",
                marginRight: "auto",
              }}
              type="integer"
              name="scoreKomputer"
              placeholder="Comp Score"
              value={this.state.bodi.scoreKomputer}
            />
          </div>

          <div className="text-center">
            <button
              onClick={(e) => this.onSubmit2(e)}
              className="btn btn-warning refresh mr-auto"
              style={{
                borderRadius: "10px",
                height: "37px",
                padding: "auto",
                font: "helvetica",
                fontWeight: "bold",
                marginRight: "auto",
              }}
            >
              SUBMIT
            </button>
          </div>

          {/* <!-- ========== END REFRESH ========== --> */}
        </section>
      </Fragment>
    );
  }
}

export default Games7;
