import React, { Fragment, useEffect, useState } from "react";
import "./styleGame.css";

const Games6 = () => {
  // 1. Score State
  const [history, setHistory] = useState({
    my_score: "",
    comp_score: "",
  });

  const { my_score, comp_score } = history;

  const onChange = (e) => {
    setHistory({ ...history, [e.name]: e.value });
  };

  // score variable
  let sPlayer = 0;
  let sKomputer = 0;

  // Computer Random Choice
  function getComp() {
    const comp = Math.random();
    if (comp < 0.34) return "BATU";
    if (comp >= 0.34 && comp < 0.67) return "GUNTING";
    return "KERTAS";
  }

  // Game Result Function
  function getHasil(pilComp, pilPlayer) {
    if (pilPlayer === pilComp) return "SERI";
    if (pilPlayer === `BATU`) return pilComp === `KERTAS` ? "KALAH" : "MENANG";
    if (pilPlayer === `GUNTING`)
      return pilComp === `KERTAS` ? "MENANG" : "KALAH";
    if (pilPlayer === `KERTAS`) return pilComp === `BATU` ? "MENANG" : "KALAH";
  }

  // Roll animation on computer choice
  function putar() {
    const putarCom = document.querySelectorAll(".listcomp li");
    let i = 0;
    const waktuMulai = new Date().getTime();
    setInterval(() => {
      if (new Date().getTime() - waktuMulai > 2000) {
        clearInterval();
        return;
      }
      putarCom[i++].classList.toggle("terpilihCom");
      if (i == putarCom.length) return (i = 0);
    }, 200);
  }

  // Update Score
  function getScore(hasil) {
    if (hasil === "SERI") {
      sPlayer = sPlayer;
      sKomputer = sKomputer;
    }
    if (hasil === "MENANG") {
      sPlayer = sPlayer + 1;
    }
    if (hasil === "KALAH") {
      sKomputer = sKomputer + 1;
    }
  }

  // Play Game
  const mainkan = () => {
    // handle picture selected
    const player = document.querySelectorAll("ul.listplayer img");
    player.forEach(function (pil) {
      pil.addEventListener("click", function apaini(el) {
        //
        // reset background from the first round
        let compTerpilih = document.querySelectorAll("ul.listcomp img");
        let arrComp = Array.from(compTerpilih);
        let playerTerpilih = document.querySelectorAll("ul.listplayer img");
        let arrPlayer = Array.from(playerTerpilih);
        arrComp.forEach((z, i) => {
          z.classList.remove("terpilih");
        });
        arrPlayer.forEach((z, i) => {
          z.classList.remove("terpilih");
        });

        // set choices
        const pilComp = getComp(); // comp schoices
        const pilPlayer = pil.className; //player choices
        const hasil = getHasil(pilComp, pilPlayer); // return result (MENANG, KALAH, SERI)

        // update result in inner HTML component
        const info = document.querySelector(".hasil");
        info.innerHTML = "";

        // give background to selected img player
        el.target.classList.add("terpilih");

        // call function to animate comp choice
        putar();

        setTimeout(() => {
          // Menampilkan background pilihan computer
          const hasilComp = document.getElementById(`${pilComp}`);
          hasilComp.classList.add("terpilih");

          // Menampilkan Hasil
          setTimeout(() => {
            // Menampilkan Hasil
            info.innerHTML = hasil;

            getScore(hasil);

            document.getElementById("myNumberP").value = sPlayer;
            document.getElementById("myNumberK").value = sKomputer;
          }, 300);
        }, 2000);
      });
    });
  };

  return (
    <Fragment>
      <section
        id="game6"
        style={{
          borderRadius: "20px",
          padding: "10px",
          backgroundColor: "rgb(230, 190, 80)",
        }}
      >
        {/* <!-- ========== NAVIGASI ========== --> */}
        <nav className="navbar navbar-expand-lg">
          <a href="/">
            <img
              src={process.env.PUBLIC_URL + "/imgGame/arrow.png"}
              style={{ width: 30, height: 30, marginRight: 20, marginLeft: 20 }}
              alt=""
            />
          </a>

          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <img
              src={process.env.PUBLIC_URL + "/imgGame/logo 1.png"}
              style={{ width: 40, height: 40, marginRight: 20 }}
              alt=""
            />
            <h3
              style={{
                fontFamily: "arial",
                fontWeight: "bolder",
                fontSize: 20,
                marginTop: 5,
                color: "rgb(97, 97, 97)",
              }}
            >
              ROCK PAPER SCISSORS
            </h3>
          </div>
        </nav>
        {/* <!-- ========== END NAVIGASI ========== --> */}

        {/* <!-- ========== MAIN PAGE ========== -->         */}
        <div className="container">
          <div className="row" style={{ marginTop: 10 }}>
            {/* <!-- == player == --> */}
            <div className="col-4 my-3">
              <div class="row">
                <div className="col-5 ml-5">
                  <h2
                    style={{
                      fontFamily: "arial",
                      fontWeight: "bolder",
                      fontSize: "calc(.7rem + 0.9vw)",
                      letterSpacing: 3,
                      textAlign: "center",
                    }}
                  >
                    ME
                  </h2>
                </div>

                <input
                  className="col-3 whiteP text-center"
                  style={{
                    height: 25,
                    width: 40,
                    fontWeight: "bold",
                    backgroundColor: "white",
                    borderRadius: "6px",
                  }}
                  type="integer"
                  name="my_score"
                  id="myNumberP"
                  placeholder=""
                  className="form-control"
                  value={my_score}
                  onChange={(e) => onChange(e)}
                />
              </div>

              <ul
                className="listplayer"
                style={{
                  listStyleType: "none",
                  textAlign: "right",
                  marginLeft: -15,
                }}
              >
                <li>
                  <img
                    onClick={() => mainkan()}
                    className="BATU"
                    src={process.env.PUBLIC_URL + "/imgGame/batu.png"}
                    alt=""
                  />
                </li>
                <li>
                  <img
                    onClick={() => mainkan()}
                    className="KERTAS"
                    src={process.env.PUBLIC_URL + "/imgGame/kertas.png"}
                    alt=""
                  />
                </li>
                <li>
                  <img
                    onClick={() => mainkan()}
                    className="GUNTING"
                    src={process.env.PUBLIC_URL + "/imgGame/gunting.png"}
                    alt=""
                  />
                </li>
              </ul>
            </div>
            {/* <!-- == end player == --> */}

            {/* <!-- == Hasil == --> */}
            <div className="col-4">
              <div
                className="hasil"
                id="hasil2"
                style={{
                  marginLeft: "15px",
                  textAlign: "center",
                  marginTop: "170px",
                }}
              >
                VS
              </div>
            </div>
            {/* <!-- == end Hasil == --> */}

            {/* <!-- == computer == --> */}
            <div className="col-4 my-3">
              <div class="row">
                <div className="col-6">
                  <h2
                    style={{
                      fontFamily: "arial",
                      fontWeight: "bold",
                      fontSize: "calc(.7rem + 0.9vw)",
                      letterSpacing: 3,
                    }}
                  >
                    COM
                  </h2>
                </div>
                <input
                  className="col-3 whiteP text-center"
                  style={{
                    height: 25,
                    width: 40,
                    fontWeight: "bold",
                    backgroundColor: "white",
                    borderRadius: "6px",
                  }}
                  type="integer"
                  name="comp_score"
                  id="myNumberK"
                  placeholder=""
                  className="form-control"
                  value={comp_score}
                  onChange={(e) => onChange(e)}
                />
              </div>

              <ul
                className="listcomp"
                style={{ listStyleType: "none", marginLeft: -35 }}
              >
                <li>
                  <img
                    id="BATU"
                    src={process.env.PUBLIC_URL + "/imgGame/batu.png"}
                    alt=""
                  />
                </li>
                <li>
                  <img
                    id="KERTAS"
                    src={process.env.PUBLIC_URL + "/imgGame/kertas.png"}
                    alt=""
                  />
                </li>
                <li>
                  <img
                    id="GUNTING"
                    src={process.env.PUBLIC_URL + "/imgGame/gunting.png"}
                    alt=""
                  />
                </li>
              </ul>
            </div>
            {/* <!-- == end computer == --> */}
          </div>
        </div>
        {/* <!-- ========== END MAIN PAGE ========== --> */}
      </section>
    </Fragment>
  );
};

export default Games6;

// {
//   /* <!-- ========== REFRESH ========== --> */
// }
// <div className="row refresh text-center m-auto">
//   <img
//     src={process.env.PUBLIC_URL + "/imgGame/refresh.png"}
//     style={{
//       marginTop: "-70px",
//       marginLeft: "10px",
//       width: "70px",
//       height: "50px",
//       cursor: "pointer",
//       zIndex: 9,
//     }}
//     alt=""
//   />
// </div>;
// {
//   /* <!-- ========== END REFRESH ========== --> */
// }
// // membuat onSubmit
// const onSubmitForm = async (e) => {
//   try {
//     const body = { my_score, comp_score };

//     const myHeaders = new Headers();
//     myHeaders.append("Content-Type", "application/json");
//     myHeaders.append("token", localStorage.token);

//     const response = await fetch(
//       `http://localhost:5000/dashboard/edithistory`,
//       {
//         method: "POST",
//         headers: myHeaders,
//         body: JSON.stringify(body),
//       }
//     );

//     const info = document.querySelector(".hasil");
//     info.innerHTML = "VS";

//     setDataChange(true);
//   } catch (err) {
//     console.error(err.message);
//   }

//   let compTerpilih = document.querySelectorAll("ul.listcomp img");
//   let arrComp = Array.from(compTerpilih);

//   let playerTerpilih = document.querySelectorAll("ul.listplayer img");
//   let arrPlayer = Array.from(playerTerpilih);

//   arrComp.forEach((z, i) => {
//     z.classList.remove("terpilih");
//   });
//   arrPlayer.forEach((z, i) => {
//     z.classList.remove("terpilih");
//   });

//   document.getElementById("myNumberP").value = 0;
//   document.getElementById("myNumberK").value = 0;
// };
