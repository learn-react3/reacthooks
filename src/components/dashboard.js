import React from "react";
import { Link } from "react-router-dom";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="text-center mt-5">
        <Link to="/game6" className="btn btn-success refresh text-center mx-3">
          Stateless Components
        </Link>
        <Link to="/game7" className="btn btn-primary refresh text-center">
          Statefull Components
        </Link>
      </div>
    );
  }
}

export default Dashboard;
